import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodProvideGUI {
    private JPanel root;
    private JButton coffeeButton;
    private JButton teaButton;
    private JButton foodButton;
    private JTextArea textArea1;
    private JButton swircleButton;
    private JButton cakeButton;
    private JButton food2Button;
    private JButton checkOutButton;
    private JTextArea textArea2;
    private JButton forHereButton;
    private JButton toGoButton;
    private JTextArea textArea3;

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodProvideGUI");
        frame.setContentPane(new FoodProvideGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
    void print(String word){
        textArea1.append(word+"\n");
    }
    void print_price(String word){
        textArea3.append(word+"\n");
    }
    int total = 0;
    void price(int price){
        double total_tax=0.0;
        if(price==-1){
            textArea1.setText("Your Order(without tax):\n");
            total = 0;
            textArea2.setText("Total Price(without tax):" + total + " yen\n");
            textArea3.setText("Your Order(without tax):\n");
        }
        else if(price==-2){
            total_tax = (double)total * 1.1;
            textArea2.setText("Total Price(in tax):" + (int)total_tax + " yen\n");
        }
        else if(price==-3){
            total_tax = (double)total * 1.08;
            textArea2.setText("Total Price(in tax):" + (int)total_tax + " yen\n");
        }
        else {
            total += price;
            textArea2.setText("Total Price(without tax):" + total + " yen\n");
        }
    }

    public FoodProvideGUI() {
        textArea1.setText("Your Order(without tax):\n");
        textArea2.setText("Total Price(without tax):0 yen");
        textArea3.setText("Your Order(without tax):\n");
        coffeeButton.setIcon(new ImageIcon(
                this.getClass().getResource("coffee.jpg")
        ));
        teaButton.setIcon(new ImageIcon(
                this.getClass().getResource("tea.jpg")
        ));
        foodButton.setIcon(new ImageIcon(
                this.getClass().getResource("original.jpg")
        ));
        swircleButton.setIcon(new ImageIcon(
                this.getClass().getResource("swircle.jpg")
        ));
        cakeButton.setIcon(new ImageIcon(
                this.getClass().getResource("cake.jpg")
        ));
        food2Button.setIcon(new ImageIcon(
                this.getClass().getResource("pizza.jpg")
        ));
    coffeeButton.addActionListener(new ActionListener() {
        @Override

        public void actionPerformed(ActionEvent e) {
            int confirmation = JOptionPane.showConfirmDialog(null, "Are you sure to order Coffee?",
                    "Order Confirmation",
                    JOptionPane.YES_NO_OPTION);
            if(confirmation==0) {
                print("Coffee");
                print_price("345yen");
                price(345);
            }
        }

    });
        teaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null, "Are you sure to order Tea?",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0) {
                    print("Tea");
                    print_price("355yen");
                    price(355);
                }
            }
        });
        foodButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null, "Are you sure to order Food?",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0) {
                    print("Food");
                    print_price("335yen");
                    price(335);
                    int confirmation2 = JOptionPane.showConfirmDialog(null, "You have a opportunity to buy Coffee for 150 yen",
                            "Order Suggestion",
                            JOptionPane.YES_NO_OPTION);
                    if(confirmation2==0){
                        print("Coffee(set)");
                        print_price("150yen");
                        price(150);
                    }
                }
            }
        });
        swircleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null, "Are you sure to order Swricle?",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0) {
                    print("Swricle");
                    print_price("550yen");
                    price(550);
                }
            }
        });
        cakeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null, "Are you sure to order Pancake?",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0) {
                    print("Pancake");
                    print_price("680yen");
                    price(680);
                    int confirmation2 = JOptionPane.showConfirmDialog(null, "You have a opportunity to buy Coffee for 150 yen",
                            "Order Suggestion",
                            JOptionPane.YES_NO_OPTION);
                    if(confirmation2==0){
                        print("Coffee(set)");
                        print_price("150yen");
                        price(150);
                    }
                }
            }
        });
        food2Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null, "Are you sure to order Pizza?",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0) {
                    print("Pizza");
                    print_price("650yen");
                    price(650);
                    int confirmation2 = JOptionPane.showConfirmDialog(null, "You have a opportunity to buy Coffee for 150 yen",
                            "Order Suggestion",
                            JOptionPane.YES_NO_OPTION);
                    if(confirmation2==0){
                        print("Coffee(set)");
                        print_price("150yen");
                        price(150);
                    }
                }

            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null, "Can I delete your order?",
                        "Delete Order",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0) {
                    price(-1);
                }
            }
        });
        forHereButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null, "Have you finished your order?",
                        "Order Finished",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0) {
                    price(-2);
                }
            }
        });
        toGoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null, "Have you finished your order?",
                        "Order Finished",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0) {
                    price(-3);
                }
            }
        });
    }
}
